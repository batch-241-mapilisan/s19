let username;
let password;
let role;

function login() {
    username = prompt("Enter your username");
    if (!username) {
        alert("Input should not be empty")
        username = prompt("Enter your username");
    }
    password = prompt("Enter your password");
    if (!password) {
        alert("Input should not be empty")
        password = prompt("Enter your password");
    }
    role = prompt("What is your role?").toLowerCase();
    switch (role) {
        case "admin":
            alert("Welcome back to the class portal, admin!");
            break;
        case "teacher":
            alert("Thank you for logging in, teacher!");
            break;
        case "student":
            alert("Welcome to the class portal, student!");
            break;
        default:
            alert("Role out of range.");
    }
}

login();

function getGrade(n1, n2, n3, n4) {
    const average = Math.round((n1 + n2 + n3 +n4) / 4);

    if (average < 75) {
        console.log(`Hello, student, your average is ${average}. The letter equivalent is F`);
    } else if (average < 80) {
        console.log(`Hello, student, your average is ${average}. The letter equivalent is D`);
    } else if (average < 85) {
        console.log(`Hello, student, your average is ${average}. The letter equivalent is C`);
    } else if (average < 90) {
        console.log(`Hello, student, your average is ${average}. The letter equivalent is B`);
    } else if (average < 96) {
        console.log(`Hello, student, your average is ${average}. The letter equivalent is A`);
    } else {
        console.log(`Hello, student, your average is ${average}. The letter equivalent is A+`);
    }
}

getGrade();